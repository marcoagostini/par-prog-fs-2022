#include <stdio.h> 
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "mpi.h" 
#include "Bitmap.h"

#define OUTPUT_FILE "juliaset.bmp"
#define IMAGE_WIDTH (4 * 1024)
#define IMAGE_HEIGHT (4 * 1024)
#define IMAGE_SCALE 1.5

typedef struct {
	double real, imag;
} Complex;

Complex multiply(Complex a, Complex b) {
	Complex c;
	c.real = a.real * b.real - a.imag * b.imag;
	c.imag = a.real * b.imag + a.imag * b.real;
	return c;
}

Complex add(Complex a, Complex b) {
	Complex c;
	c.real = a.real + b.real;
	c.imag = a.imag + b.imag;
	return c;
}

int julia_value(int x, int y) {
	double jx = IMAGE_SCALE * ((double)IMAGE_WIDTH / 2 - x) / (IMAGE_WIDTH / 2);
	double jy = IMAGE_SCALE * ((double)IMAGE_HEIGHT / 2 - y) / (IMAGE_HEIGHT / 2);

	Complex c, a;
	c.real = -.8f;
	c.imag = .156f;
	a.real = jx;
	a.imag = jy;
	
	int i;
	for (i = 0; i < 200 && a.real * a.real + a.imag * a.imag <= 1000; i++) {
		a = add(multiply(a, a), c);
	}
	return i;
}

void compute_julia_set(int* pixels, int rank, int sectionLength) {
	int i;
	for (i = rank * sectionLength; i < ((rank+1) * sectionLength) && i < (IMAGE_WIDTH * IMAGE_HEIGHT); i++) {
		pixels[i] = julia_value(i % IMAGE_WIDTH, i / IMAGE_WIDTH);  //col = i % IMAGE_WIDTH, row = i / IMAGE_WIDTH
	}
}

int main(int argc, char* argv[]) {
	double totalTime = 0;
	int repetitions = 10;
	clock_t start;
	clock_t end;
	double elapsed_time;
	int i;
	
	MPI_Init(&argc, &argv);
	int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

	for (i = 0; i < repetitions; i++){
		start = clock();
		
		int sectionLength = (int)((IMAGE_WIDTH * IMAGE_HEIGHT / size) + 1);  //+1 for 'ceiling' of section length
		int* pixels = (int*)malloc(IMAGE_HEIGHT * IMAGE_WIDTH * sizeof(int));
		compute_julia_set(pixels, rank, sectionLength);
	
		if(rank == 0){
			int process;
			for (process = 1; process < size; process++){
				MPI_Recv(pixels + (process * sectionLength), sectionLength, MPI_INT, process, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);  //offset = process * sectionLength
			}
			end = clock();
			elapsed_time = ((double)end - start) * 1000 / (double)CLOCKS_PER_SEC;
			printf("Computation %d: %0.f ms\n", i, elapsed_time);
			totalTime += elapsed_time;
		} else {
			MPI_Send(pixels + (rank * sectionLength), sectionLength, MPI_INT, 0, 0, MPI_COMM_WORLD);  //offset = rank * sectionLength
		}
		free(pixels);
	}
	
	if (rank == 0){
		printf("Average total computation: %0.f ms\n", (totalTime/repetitions)+3); // '+3' for the initialization of the MPI_Comm_* which take ~3ms
	}

	MPI_Finalize();
	return 0;
}

