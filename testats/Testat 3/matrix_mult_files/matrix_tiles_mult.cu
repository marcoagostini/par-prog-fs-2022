#include "cuda_helper.cuh"

// C = A * B

const int C_ROWS = 1000;
const int C_COLS = 2000;
const int A_COLS = 1500;

const int A_ROWS = C_ROWS;
const int B_ROWS = A_COLS;
const int B_COLS = C_COLS;

const int nofElemA = A_ROWS * A_COLS;
const int nofElemB = B_ROWS * B_COLS;
const int nofElemC = C_ROWS * C_COLS;

const size_t sizeA = nofElemA * sizeof(float);
const size_t sizeB = nofElemB * sizeof(float);
const size_t sizeC = nofElemC * sizeof(float);

const int TILE_SIZE = 16;

__global__
void MatrixMultTilesKernel(float *A, float *B, float *C){
	__shared__ float Asub[TILE_SIZE][TILE_SIZE];
	__shared__ float Bsub[TILE_SIZE][TILE_SIZE];

	int tx = threadIdx.x, ty = threadIdx.y;
	int col = blockIdx.x * TILE_SIZE + tx;
	int row = blockIdx.y * TILE_SIZE + ty;
	int nofTiles = (A_COLS + TILE_SIZE - 1) / TILE_SIZE;

	float sum = 0.0;

	for (int tile = 0; tile < nofTiles; tile++) {
		// Loading of the different values in the tile into shared memory
		if (row < A_ROWS && tile * TILE_SIZE + tx < A_COLS) 
		{
			Asub[ty][tx] = A[row * A_COLS + tile * TILE_SIZE + tx];
		}
		// Loading of the different values in the tile into shared memory
		if (col < B_COLS && tile * TILE_SIZE + ty < B_ROWS) 
		{
			Bsub[ty][tx] = B[(tile * TILE_SIZE + ty) * B_COLS + col];
		}
		// Sync for both tiles to be loaded
		__syncthreads();
		if (row < C_ROWS && col < C_COLS) 
		{
			for (int ksub = 0; ksub < TILE_SIZE; ksub++) 
			{
				if (tile * TILE_SIZE + ksub < A_COLS) 
				{
					sum += Asub[ty][ksub] * Bsub[ksub][tx];
				}
			}
		}
		
		__syncthreads();
		if (row < C_ROWS && col < C_COLS) {
			C[row * C_COLS + col] = sum;
		}
	}
}

void cudaMatrixTilesMult(float *A, float *B, float *C, int repetitions, bool warmup, FILE *file, bool printToFile) {
	clock_t startClock = clock();

	float hostToDevice = 0;
	float compute = 0;
	float deviceToHost = 0;
	float durationMilliseconds;

	dim3 threadsPerBlock(TILE_SIZE, TILE_SIZE);
	dim3 blocksPerGrid((C_COLS + TILE_SIZE - 1) / TILE_SIZE, (C_ROWS + TILE_SIZE - 1) / TILE_SIZE);

	float *d_A, *d_B, *d_C;
	cudaEvent_t start;
	cudaEvent_t stop;

	handleCudaError(cudaMalloc(&d_A, sizeA));
    handleCudaError(cudaMalloc(&d_B, sizeB));
    handleCudaError(cudaMalloc(&d_C, sizeC));

	handleCudaError(cudaEventCreate(&start));
	handleCudaError(cudaEventCreate(&stop));

	for (int i = 0; i < repetitions; i++)
	{
		handleCudaError(cudaEventRecord(start));
		handleCudaError(cudaMemcpy(d_A, A, sizeA, cudaMemcpyHostToDevice));
    	handleCudaError(cudaMemcpy(d_B, B, sizeB, cudaMemcpyHostToDevice));
		handleCudaError(cudaEventRecord(stop));
		handleCudaError(cudaEventSynchronize(stop));
		handleCudaError(cudaEventElapsedTime(&durationMilliseconds, start, stop));
		hostToDevice += durationMilliseconds;

		handleCudaError(cudaEventRecord(start));
		MatrixMultTilesKernel<<<blocksPerGrid, threadsPerBlock>>>(d_A, d_B, d_C);
		handleCudaError(cudaGetLastError());
		handleCudaError(cudaEventRecord(stop));
		handleCudaError(cudaEventSynchronize(stop));
		handleCudaError(cudaEventElapsedTime(&durationMilliseconds, start, stop));
		compute += durationMilliseconds;

		handleCudaError(cudaEventRecord(start));
		handleCudaError(cudaMemcpy(C, d_C, sizeC, cudaMemcpyDeviceToHost));
		handleCudaError(cudaEventRecord(stop));
		handleCudaError(cudaEventSynchronize(stop));
		handleCudaError(cudaEventElapsedTime(&durationMilliseconds, start, stop));
		deviceToHost += durationMilliseconds;
	}

	handleCudaError(cudaEventDestroy(start));
	handleCudaError(cudaEventDestroy(stop));
	handleCudaError(cudaFree(d_A));
    handleCudaError(cudaFree(d_B));
    handleCudaError(cudaFree(d_C));

	float total = float(clock() - startClock) / (CLOCKS_PER_SEC * repetitions);
	
	if (!warmup)
	{
		if(printToFile){
			fprintf(file, "CUDA: %.3lf seconds\n", total);
			fprintf(file, "CUDA: Copy input to device: %.3lf seconds\n", hostToDevice / (1000 * repetitions));
			fprintf(file, "CUDA: Compute time: %.3lf seconds\n", compute / (1000 * repetitions));
			fprintf(file, "CUDA: Copy output to host: %.3lf seconds\n", deviceToHost / (1000 * repetitions));
		}
		printf("CUDA: %.3lf seconds\n", total);
		printf("CUDA: Copy input to device: %.3lf seconds\n", hostToDevice / (1000 * repetitions));
		printf("CUDA: Compute time: %.3lf seconds\n", compute / (1000 * repetitions));
		printf("CUDA: Copy output to host: %.3lf seconds\n", deviceToHost / (1000 * repetitions));
	}
}

__global__
void MatrixMultKernel(float *A, float *B, float *C){
	int col = threadIdx.x + blockIdx.x * blockDim.x;
	int row = threadIdx.y + blockIdx.y * blockDim.y;
	if(col < C_COLS && row < C_ROWS){
		float sum = 0.0;
		for (int k = 0; k < A_COLS; k++) {
			sum += A[row * A_COLS + k] * B[k * B_COLS + col];
		}
		C[row * C_COLS + col] = sum;
	}
}

void cudaMatrixMult(float *A, float *B, float *C, int repetitions, bool warmup, FILE *file, bool printToFile) {
	clock_t startClock = clock();
	float hostToDevice = 0;
	float compute = 0;
	float deviceToHost = 0;
	float durationMilliseconds;

	dim3 blockDim(32,32);
    int blocksX = (C_COLS % blockDim.x == 0) ? C_COLS / blockDim.x : C_COLS / blockDim.x + 1;
	int blocksY = (C_ROWS % blockDim.y == 0) ? C_ROWS / blockDim.y : C_ROWS / blockDim.y + 1;
	dim3 gridDim(blocksX, blocksY);
	float *d_A, *d_B, *d_C;
	cudaEvent_t start;
	cudaEvent_t stop;

	handleCudaError(cudaMalloc(&d_A, sizeA));
    handleCudaError(cudaMalloc(&d_B, sizeB));
    handleCudaError(cudaMalloc(&d_C, sizeC));

	handleCudaError(cudaEventCreate(&start));
	handleCudaError(cudaEventCreate(&stop));
	
	for (int i = 0; i < repetitions; i++)
	{
		handleCudaError(cudaEventRecord(start));
		handleCudaError(cudaMemcpy(d_A, A, sizeA, cudaMemcpyHostToDevice));
    	handleCudaError(cudaMemcpy(d_B, B, sizeB, cudaMemcpyHostToDevice));
		handleCudaError(cudaEventRecord(stop));
		handleCudaError(cudaEventSynchronize(stop));
		handleCudaError(cudaEventElapsedTime(&durationMilliseconds, start, stop));
		hostToDevice += durationMilliseconds;

		handleCudaError(cudaEventRecord(start));
		MatrixMultKernel<<<gridDim, blockDim>>>(d_A, d_B, d_C);
		handleCudaError(cudaEventRecord(stop));
		handleCudaError(cudaEventSynchronize(stop));
		handleCudaError(cudaEventElapsedTime(&durationMilliseconds, start, stop));
		compute += durationMilliseconds;

		handleCudaError(cudaEventRecord(start));
		handleCudaError(cudaMemcpy(C, d_C, sizeC, cudaMemcpyDeviceToHost));
		handleCudaError(cudaEventRecord(stop));
		handleCudaError(cudaEventSynchronize(stop));
		handleCudaError(cudaEventElapsedTime(&durationMilliseconds, start, stop));
		deviceToHost += durationMilliseconds;
	}

	handleCudaError(cudaEventDestroy(start));
	handleCudaError(cudaEventDestroy(stop));
	handleCudaError(cudaFree(d_A));
    handleCudaError(cudaFree(d_B));
    handleCudaError(cudaFree(d_C));

	float total = float(clock() - startClock) / (CLOCKS_PER_SEC * repetitions);

	if (!warmup)
	{
		if(printToFile){
			fprintf(file, "CUDA: %.3lf seconds\n", total);
			fprintf(file, "CUDA: Copy input to device: %.3lf seconds\n", hostToDevice / (1000 * repetitions));
			fprintf(file, "CUDA: Compute time: %.3lf seconds\n", compute / (1000 * repetitions));
			fprintf(file, "CUDA: Copy output to host: %.3lf seconds\n", deviceToHost / (1000 * repetitions));
		}
		printf("CUDA: %.3lf seconds\n", total);
		printf("CUDA: Copy input to device: %.3lf seconds\n", hostToDevice / (1000 * repetitions));
		printf("CUDA: Compute time: %.3lf seconds\n", compute / (1000 * repetitions));
		printf("CUDA: Copy output to host: %.3lf seconds\n", deviceToHost / (1000 * repetitions));
	}
}

void fillRandomArray(float *A, int numElements) {
	for (int i = 0; i < numElements; i++) {
		A[i] = rand() / (float)RAND_MAX;
	}
}

void verifyResults(float *A, float *B, float *C) {
	printf("Verifying ...");
	for (int row = 0; row < C_ROWS; row++) {
		for (int col = 0; col < C_COLS; col++) {
			float sum = 0.0;
			for (int k = 0; k < A_COLS; k++) {
				sum += A[row * A_COLS + k] * B[k * B_COLS + col];
			}
			if (fabsf(C[row * C_COLS + col] - sum) > 1e-3f) {
				fprintf(stderr, "Result verification failed at element %d: %f vs. %f!\n", row, C[row * C_COLS + col], sum);
				exit(EXIT_FAILURE);
			}
		}
	}
	printf(" done\n");
}

void sequentialMatrixMult(float *A, float *B, float *C, FILE *file, bool printToFile) {
	clock_t start = clock();

	for (int row = 0; row < C_ROWS; row++) {
		for (int col = 0; col < C_COLS; col++) {
			float sum = 0.0;
			for (int k = 0; k < A_COLS; k++) {
				sum += A[row * A_COLS + k] * B[k * B_COLS + col];
			}
			C[row * C_COLS + col] = sum;
		}
	}

	float diff = float(clock() - start) / CLOCKS_PER_SEC;
	if(printToFile){fprintf(file, "Seq: %.3lf seconds\n", diff);}
	printf("Seq: %.3lf seconds\n", diff);
}

int main() {
	int repetitions = 100;
	bool printToFile = true;
	
	FILE *f = NULL;

	float *h_A = (float *)malloc(sizeA);
	handleAllocationError(h_A);
	fillRandomArray(h_A, nofElemA);

	float *h_B = (float *)malloc(sizeB);
	handleAllocationError(h_B);
	fillRandomArray(h_B, nofElemB);
	
	float *h_C = (float *)malloc(sizeC);
	handleAllocationError(h_C);

	if(printToFile){f = fopen("output.txt", "a");}
	
	if(printToFile){fprintf(f, "\n---------- Sequential ----------\n");}
	printf("\n---------- Sequential ----------\n");
	sequentialMatrixMult(h_A, h_B, h_C, f, printToFile);

	cudaMatrixMult(h_A, h_B, h_C, 2, true, f, printToFile);
	if(printToFile){fprintf(f, "\n---------- MatrixMult (avg of %d rep.) ----------\n", repetitions);}
	printf("\n---------- MatrixMult (avg of %d rep.) ----------\n", repetitions);
	cudaMatrixMult(h_A, h_B, h_C, repetitions, false, f, printToFile);
	verifyResults(h_A, h_B, h_C);
	
	cudaMatrixTilesMult(h_A, h_B, h_C, 2, true, f, printToFile);
	if(printToFile){fprintf(f, "\n---------- MatrixTileMult (avg of %d rep.) ----------\n", repetitions);
		fprintf(f, "---------- Tile-Size: %d \n", TILE_SIZE);}
	printf("\n---------- MatrixTileMult (avg of %d rep.) ----------\n", repetitions);
	printf("---------- Tile-Size: %d \n", TILE_SIZE);
	cudaMatrixTilesMult(h_A, h_B, h_C, repetitions, false, f, printToFile);
	verifyResults(h_A, h_B, h_C);

	fclose(f);
	free(h_A);
	free(h_B);
	free(h_C);

	return 0;
}
