\section{Thread Synchronisation}
Ohne Vorkehrungen laufen Threads beliebig verzahnt oder parallel. Oft muss die Nebenläufigkeit des Threads beschränkt bzw. Synchronisiert werden, damit die korrekte Nebenläufigkeit garantiert werden kann.

\subsection{Gemeinsame Ressourcen}
Threads teilen sich den Adressraum sowie den Heap. Somit können Objekte auf dem Heap, zum Beispiel Objekte von mehreren Thread verwendet werden.

\paragraph{Race Condition} \hfill \\
Eine Race-Condition (Rennbedingung) ist ein Zustand, in dem die Ausführung von Programmcode oder Prozessen von der Reihenfolge abhängt, in der bestimmte Ereignisse oder Zugriffe auf gemeinsam genutzte Ressourcen stattfinden. Dies kann zu unerwartetem Verhalten oder Fehlern im Programm führen, da der erwartete Ablauf nicht garantiert werden kann. 

Der Zugriff auf die balance variable ist nicht atomar. Der Zugriff besteht aus mehreren nicht atomaren Operationen.  In der Regel ist der analysierte Fehler nicht zu sehen, aber es besteht die Möglichkeit, dass dieser Fall eintritt.

Der Code in \autoref{fig:racecondition} wird auf Instruktionsebene (Bytecode/Assembler) nicht atomar sein.

\begin{figure}[H]
  \center
  \includegraphics[width=0.65\textwidth]{racecondition}
  \caption{Race Condition in Java}
  \label{fig:racecondition}
\end{figure}

\begin{figure}[H]
  \center
  \includegraphics[width=0.5\textwidth]{raceconditionszenario}
  \caption{Race Condition Szenario in Java}
\end{figure}

\paragraph{Kritischer Abschnitt (Critical Section)} \hfill \\
Der Kritische Abschnitt dieser kleinen Applikation ist in \lstinline|deposit()|. Dieser Teil darf nur von einem Thread zur gleichen Zeit ausführbar sein. Dies kann mit einem \textbf{gegenseitigen Ausschluss (Mutual Exclusion)} realisiert werden. 

\pagebreak

\subsubsection{Naiver Ansatz}
Wir könnten versuchen mit einem einfachen boolean einen Lock auf dem Account zu implementieren. Dieser Ansatz funktioniert nicht! Die Schleife und die Zuweisung sind nicht atomar!

\begin{lstlisting}
class BankAccount {
  private int balance = 0;
  private boolean locked = false;
  public void deposit(int amount) {
    while (locked) { } // busy waiting loop
    locked = true;     // enter critical section
    this.balance += amount;
    locked = false; // exit critical section
  }
}
\end{lstlisting}

\begin{figure}[H]
  \center
  \includegraphics[width=0.5\textwidth]{naiverlock}
  \caption{Naiver Lock von der Klasse}
\end{figure}

\subsection{Gegenseitiger Ausschluss}
Der gegenseitige Ausschluss (engl. mutual exclusion) stellt sicher, dass nur ein Prozess oder Thread zu einem bestimmten Zeitpunkt auf eine gemeinsam genutzte Ressource zugreifen kann. Dadurch wird verhindert, dass Race-Conditions auftreten und die Integrität der Ressource gewährleistet. Die korrekte Implementierung von gegenseitigem Ausschluss ist nicht trivial und es gibt verschiedene theoretische Konzepte, die dafür verwendet werden können. Zusätzlich müssen sie auch Weak Memory Consistency beachten. Hierzu werden meist vorgefertigte Synchronisationsmechanismen verwendet.

\subsubsection{Java Synchronized Methoden (Monitor)}
Der synchronized ist ein modifier für Methoden. Der Body der Methode ist ein kritischer Abschnitt und wird nur unter gegenseitigem Ausschluss durchgeführt.

\paragraph{Funktionsweise} \hfil \\
Jedes \textit{Objekt} hat einen Lock (Monitor-Lock). Maximal ein Thread kann denselben Lock gaben. Ein synchronized Code Block belegt den Lock des jeweiligen Objektes. Beim Eintritt wird das Lock besetzt, ansonsten wird gewartet bis es frei ist. Bei Austritt wird das Lock wieder freigegeben.

Der Lock gilt für das jeweilige Objekt und nicht für die Funktion o.ä.!

\begin{lstlisting}
class BankAccount {
  private int balance = 0;

  public synchronized void deposit(int amount) { // start critical part
    this.balance += amount; 
  } // end critical part
}
\end{lstlisting}

\pagebreak

\subsubsection{Synchronized Statement Block}
Ermöglicht die explizite Angabe, auf welcher Instanz blockiert wird. Das Lock wird bei jedem Exit freigegeben (Ende des Blocks, Return Statement, Unbehandelte Exception).

\begin{lstlisting}
class BankAccount {
	private int balance = 0;
		
	public void deposit(int amount) {
  		synchronized(this) { // Explizit Lock for this 
    	this.balance += amount; // critical section
	}
	System.out.println("Deposit done"); }
}
\end{lstlisting}

\subsubsection{Exit aus Synchronized Block}
Das Objekt/Class Lock wird bei jedem Exit wieder freigegeben: Ende des Blocks, Return Statement oder unbehandelte Exception.

\subsection{Monitor Konzept}
Der Monitor ist ein Objekt mit internem gegenseitigen Ausschluss. Nur ein Thread operiert zur gleichen Zeit im Monitor. Alle äusseren Methoden sind synchronized. Über einen Wait- und Singal-Mechanismus können Threads im Monitor auf Bedingung warten und wartende Threats aufwecken (signalisieren).

\begin{itemize}
	\item + Sehr mächtiges Konzept
	\SubItem{Jede Synchronisation realisierbar}
	\SubItem{Objekt-orientiert}
	\item - Nicht immer optimal
		\SubItem{Effizienprobleme (notifyAll() bei mehreren Conditions)}
		\SubItem{Fairnessprobelme (Signal and Continue, kein FIFO)}
\end{itemize}

\begin{lstlisting}
class BankAccount {
  private int balance = 0;
	
	public synchronized void withdraw(int amount) throws 	InterruptedException {
    	while (amount > balance) {
			wait(); // wait for 
		}		balance -= amount;
	}
	public synchronized void deposit(int amount) {
  		balance += amount;
    	notifyAll(); // call all waiting threads waiting
	} 
}
\end{lstlisting}

\paragraph{Wie funktioniert der Monitor?} \hfill \\
Das \lstinline|wait()| gibt Monitor-Lock temporär frei, damit ein anderer Thread die Bedingung im Monitor erfüllen kann.

\subsubsection{Wecksignal}
Das Wecksignal signalisiert eine Bedingung im Monitor. Die \lstinline|notify()|. Methode weckt einen beliebigen wartenden Threat im Monitor. Das \lstinline|notifyAll()| weckt alle im Monitor wartenden Threats und hat keinen Effekt, wenn kein Threat in Warteraum ist.

Es wird keine Unterscheidung zwischen verschiedene semantischen Bedingung gemacht. Wartende müssen selbst schauen, ob sie ein Signal interessiert oder nicht.

\paragraph{Gründe zum Aufwachen aus wait} \hfill 
\begin{itemize}
  \item \lstinline|notifyAll(), notify()|
  \item InterruptedException
  \item Spurious Wakeup - Fälschliches Aufwecken in vereinzelten Betriebssystemen, Schlechtes Design: OS Implementierung vereinfacht dafür Benutzung kompliziert.
\end{itemize}

\begin{figure}[H]
  \center
  \includegraphics[width=0.5\textwidth]{monitorfunktion}
  \caption{Funktionsweise des Monitors}
\end{figure}

\subsubsection{Pitfalls}
\paragraph{Falle 1: Wait mit if} \hfill \\
Der aufgeweckte Thread muss neu um den Monitor-Eintritt kämpfen. Ein anderer Thread kann vorher in den Monitor und seine Bedingung validieren. Die Wartebedingung muss in einer Schlafe getestet werden!

\paragraph{Falle 2: Single Notify} \hfill \\
Der Thread hat mehrere semantische Wartebedingungen und die notify() Funktion weckt einen beliebigen Thread im Warteraum auf. Der aufgeweckte Thread wartet aber evtl. auf eine andere Bedingung und es kommt kein Thread, der weitermachen kann. Es sollte je nach Anforderungen ein notifyAll() verwendet werden.

\begin{lstlisting}
class BoundedBuffer<T> {
	private Queue<T> queue = new LinkedList<>();
	private int limit = 1; // or initialize in constructor
	
	public synchronized void put(T item) throws InterruptedException {
		while (queue.size() == limit) {
			wait(); // await non-full
		}
  		queue.add(item);
  		notifyAll();   // signal non-empty
	}
	
	public synchronized T get() throws InterruptedException {
    	InterruptedException (queue.size() == 0) {
     		wait();   // await non-empty
	}
    var item = queue.remove();
    notifyAll();   // signal non-full
    return item;
  }
}
\end{lstlisting}
