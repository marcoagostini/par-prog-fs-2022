\section{Thread Pools}
Threads sind teuer und viele Threads verlangsamen das System. Die Wartezeit nimmt mit mehreren Threads bedeutend zu. Zusätzlich sind auch die Speicherkosten hoch, weil jeder Thread seinen eigenen Stack hat und pro Thread ein volles Register-Backup benötigt wird bei einer Preemption (temporäre Unterbrechung).

Die Lösungsidee für diese Probleme ist die Modellierung einer hohen Parallelität (Problem Space) aber eine beschränkte Anzahl Threads (Machine Space). Die Threads müssen alle Prozessoren auslasten und wir müssen mindestens so viele Threads wie Prozessoren haben. 

\subsection{Task}
Tasks definieren potentiell parallele Arbeitsschritte und könnnen im Thread Pool ausgeführt werden. Task können parallel laufen, müssen aber nicht. \textbf{Run to completion:} Der Task muss zu ende laufen, bevor der Worker Thread einen anderen Task ausführen kann.

\subsection{Thread Pool}
Tasks werden in die Warteschlange eingereiht und auf eine beschränkte Anzahl an worker Threads verteilt. Die Worker Threads holen die Tasks aus der Warteschlange und führen diese aus. Eine Ausnahme sind dabei die geschachtelten Tasks (Sub-Tasks). 

\textbf{Einschränkung:} die Tasks dürfen nicht aufeinander warten (ausser Sub-Tasks).

\begin{figure}[H]
  \center
  \includegraphics[width=0.75\textwidth]{threadpool}
  \caption{Thread Pool}
\end{figure}

\subsubsection{Skalierende Performance}
Um dieses Konzept richtig zu verwenden, sollten Programme mit Tasks modelliert werden. Die Tasks laufen automatisch schneller auf parallelen Maschinen und schöpfen die Parallelität aus ohne die Kosten für die Neuerstellung von Threads. Die Anzahl Threads pro System ist konfigurierbar. Wir sparen uns die Kosten für den Aufbau neuer Threads, weil wir konsistent immer die gleichen werden.

\begin{verbatim}
	# Worker Threads = # Prozessoren + # Pendente I/O-Aufrufe
\end{verbatim}


\subsubsection{Java}
\textbf{Java Task Lancierung}
\begin{lstlisting}
var threadPool = new ForkJoinPool();

Future<Integer> future = threadPool.submit(() -> {
	int value = ...;
	// long calculation
	return value;
});
\end{lstlisting}

\subsection{Future}
Ein Future in Java ist ein Objekt, das verwendet wird, um auf das Ergebnis einer asynchronen Operation zu warten. Es ermöglicht es einem Programm, mit anderen Aufgaben fortzufahren, während eine lange laufende Operation im Hintergrund ausgeführt wird. Sobald die Operation abgeschlossen ist, kann das Ergebnis über das Future-Objekt abgerufen werden. Es gibt auch die Möglichkeit, Callbacks mit Future zu registrieren, die aufgerufen werden, wenn die asynchrone Aufgabe abgeschlossen ist.

\begin{lstlisting}
Future<T> future = threadPool.submit(...);

T result = future.get(); // blockiert, bis der Task beendet ist
\end{lstlisting}

\paragraph{Anwendung}
Unsere Applikation berechnet die Prim-Zahlen von $[2:N]$.

\begin{lstlisting}
int counter = 0; 
for (int number = 2; number < N; number++) {
	if (isPrime(number)) { counter++; }
}
\end{lstlisting}

Diese Anwendung kann primitiv parallelisiert werden, in dem wir zwei Tasks oder Threads verwenden und das Ergebnis wieder zusammenführen.

\begin{lstlisting}
var left = threadPool.submit(() -> count(leftPart));
var right = threadPool.submit(() -> count(rightPart));
result = left.get() + right.get();
\end{lstlisting}


\subsection{Subtasks}
In Java sind rekursive Tasks eine Möglichkeit, parallele Rechenaufgaben durchzuführen. Sie ermöglichen es, eine Aufgabe in kleinere Teilaufgaben zu unterteilen, die dann unabhängig voneinander auf mehreren Prozessorkernen ausgeführt werden können.

Ein Beispiel für eine rekursive Aufgabe wäre ein Quicksort-Algorithmus, bei dem eine große Liste von Elementen in kleinere Unterlisten unterteilt wird, die dann unabhängig voneinander sortiert werden. Sobald alle Unterlisten sortiert sind, werden sie zu einer sortierten Gesamtliste zusammengefügt.

In Java werden rekursive Tasks durch die Verwendung der Klasse RecursiveTask implementiert, die von der Klasse ForkJoinTask erbt. Mit dieser Klasse kann man eine Aufgabe in kleinere Teilaufgaben unterteilen und diese dann parallel ausführen lassen.

\begin{figure}[H]
  \center
  \includegraphics[width=0.6\textwidth]{subtasks}
  \caption{Rekursive Tasks}
\end{figure}

\pagebreak

\begin{lstlisting}
class CountTask extends RecursiveTask<Integer> {
	private final int lower, upper;
	
	public CountTask(int lower, int upper) {
		this.lower = lower; 
		this.upper = upper; 
	}
	protected Integer compute() {
		if (lower == upper) { return 0; }
		if (lower + 1 == upper) { return isPrime(lower) ? 1 : 0; }
		
		int middle = (lower + upper) / 2;
		var left = new CountTask(lower, middle);
		var right = new CountTask(middle, upper);
		left.fork(); // starten des subtask in eigenem Thread
		right.fork(); // starten des subtask in eigenem Thread
		return right.join() + left.join(); // joinen der Resultate
	}
}
\end{lstlisting}

\subsection{.NET Task Parallel Library (TPL)}
Die .NET Task Parallel Library (TPL) ist eine Bibliothek in der .NET Framework, die es Entwicklern ermöglicht, parallele und asynchrone Aufgaben einfach zu erstellen und auszuführen. Die TPL ermöglicht es, parallele und asynchrone Aufgaben mithilfe von Threads, Task-Objekten und Threadpools zu verwalten und zu steuern.

\subsubsection{Task}
In der .NET Task Parallel Library (TPL) ist ein Task ein Objekt, das verwendet wird, um parallele und asynchrone Aufgaben auszuführen. Ein Task repräsentiert eine einzelne, unabhängige Aufgabe, die parallel zu anderen Aufgaben ausgeführt werden kann.

\begin{lstlisting}
Task task = Task.run(() => {
	// task implementation
});
// perform other activity
task.Wait();

// Task mit Rueckgabewert
Task<int> task = Task.Run(() => {
	int total = ... 
	// some calcualtion
	return total;
});
Console.Write(task.result); // blockiert bis an das Task-Ende
\end{lstlisting}

\paragraph{Geschachtelte Tasks} \hfill \\
Tasks können Sub-Tasks starten und/oder abwarten. Hier ist kein spezieller ForkJoinTask nötig.

\begin{lstlisting}
Task.Run(() => {
	...
	var left = Task.Run(() => Count(leftPart));
	var right = Task.Run(() => Count(rightPart));
	int result = left.Result + right.Result;
	...
});
\end{lstlisting}

\subsubsection{Parallele Statements}
Ist eine Menge an Statements, welche wir potentiell parallel ausführen können. Die Tasks haben eine Barriere am Ende.

\begin{minipage}{0.5\textwidth}
\begin{lstlisting}
	Parallel.invoke(
	() => MergeSort(l, m);
	() => MergeSort(m, r);
);
\end{lstlisting}	
\end{minipage}
\begin{minipage}{0.5\textwidth}
	\centering
	\includegraphics[width=0.5\textwidth]{parallelestatements}
\end{minipage}

\subsubsection{Parallel Loop}
Die Schlaufen-Bodies können potentiell parallel ausgeführt werden. Die Bodies werden in Tasks gruppiert und so auch ausgeführt. Am Ende dieser Task. haben wir eine Barriere, die blockiert.

\begin{minipage}{0.5\textwidth}
\begin{lstlisting}
for (i = 0; i < array.Length; i++) {
	DoComputation(array[i]);
}
// Parallelisierte Version
Paralle.ForEach(list, 
	file => Convert(file)
);
\end{lstlisting}	
\end{minipage}
\begin{minipage}{0.5\textwidth}
	\centering
	\includegraphics[width=0.5\textwidth]{parallelerloop}
\end{minipage}

\paragraph{Parallel For} \hfill 
\begin{lstlisting}
Parallel.For(0, array.Length,
	i => DoComputation(array[i])
);
\end{lstlisting}

\paragraph{Parallele Loop Partitionierung} \hfill \\
Schlaufen mit sehr kurzen Bodies wären sehr ineffizient, wenn jeder Iteration nur einen parallelen Task ausführen würde. Die TPL gruppiert automatisch mehrere Bodies zu einem Task.

\begin{figure}[H]
  \center
  \includegraphics[width=0.5\textwidth]{tlpgruppierung}
  \caption{TLP automatische Gruppierung}
\end{figure}

\pagebreak

\begin{figure}[H]
  \center
  \includegraphics[width=0.7\textwidth]{partitionierungloops}
  \caption{Aufteilung von Loops/Invoke auf Worker Threads}
\end{figure}

\subsubsection{Parallel LINQ (PLINQ)}
Parallelisiert die Abfragen mit der Language-Integrates Query Language. Ist das Pendant zur Java Stream API.

\begin{lstlisting}
from book in bookCollection.AsParallel()
	where book.Title.Contains("Concurrency")
	selectbook.ISBN // Reihenfolge beliebig
	
from number in inputList.AsParallel().AsOrdered() 
	select IsPrime(number) // Reihenfolge erhalten
\end{lstlisting}

\subsubsection{Work Stealing Thread Pool}
Ein Work Stealing Thread Pool ist eine spezielle Art von Thread Pool, bei dem die Threads untereinander Aufgaben "stehlen" können, um ihre Ausführungszeit effektiver zu nutzen. Ein Work Stealing Thread Pool ermöglicht es den Threads, die Warteschlangen anderer Threads zu durchsuchen und Aufgaben zu "stehlen", die noch nicht bearbeitet wurden. Dadurch wird sichergestellt, dass immer alle Threads voll ausgelastet sind und keine Zeit verschwendet wird.

\begin{figure}[H]
  \center
  \includegraphics[width=0.7\textwidth]{workstealing}
  \caption{Work Stealing Thread Pool}
\end{figure}

\subsubsection{Thread Injection}
Die TPL fügt zur Laufzeit neue Worker Threads hinzu. Dies kann mit der Hilfe vom Hill Climbing Algorithmus realisiert werden. Dieser misst den Durchsatz und variiert die Anzahl Worker Threads und untersucht, ob der Durchsatz besser wird.

Generiert kein Deadlock bei Task-Abhängigkeiten im Gegensatz zum Java ForkJoinPool. Deadlocks sind mit (ThreadPool.SetMaxThreads()) immer noch möglich.
