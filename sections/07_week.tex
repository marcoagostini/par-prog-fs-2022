\section{Memory Models}
Memory Models sind Regeln, die beschreiben, wie die Zugriffe auf gemeinsam genutzte Speicherbereiche in einer parallelen Programmierung synchronisiert werden sollen. Sie definieren, wann die Schreibzugriffe von einem Thread auf den Speicher sichtbar sind und wann die Lesezugriffe von einem anderen Thread den aktuellen Wert des Speichers sehen. Ein Beispiel für ein Memory Model ist das "sequential consistency model", bei dem jeder Thread die Schreibzugriffe der anderen Threads in der gleichen Reihenfolge wahrnimmt, in der sie ausgeführt wurden.

\textbf{Lock-Freie Programmierung:} Korrekte nebenläufige Interaktion ohne Locks, Garantien des Speichermodells nutzen mit dem Ziel der effizienten Synchronisation.

\begin{figure}[H]
  \center
  \includegraphics[width=0.5\textwidth]{beispiel}
  \caption{Weak Consitency}
\end{figure}

\paragraph{Ursachen für die Probleme}
\textbf{Weak Consistency}
Speicherzugriffe werden in verschiedenen Reihenfolgen von verschiedenen Threads gesehen. Ausnahme: Synchronisationen/Speicherbarrieren

\textbf{Optimierungen}
Compiler, Laufzeitsystem und CPUs, und Instruktionen werden umgeordnet und wegoptimiert.

\subsection{Java Memory Model (JMM)}
Das Java Memory Model (JMM) beschreibt, wie die Zugriffe auf gemeinsam genutzten Speicher in Java-Programmen synchronisiert werden und wie die Schreib- und Lesezugriffe von Threads koordiniert werden. Es definiert, wann Schreibzugriffe von einem Thread auf den Speicher sichtbar sind und wann Lesezugriffe von anderen Threads den aktuellen Wert des Speichers sehen.

Minimal spezifizierbare Garantien:
\begin{itemize}
	\itemsep -0.2em
	\item Atomicity (Unteilbarkeit) 
	\item Visibility (Sichtbarkeit)
	\item Ordering (Reihenfolge)
\end{itemize}

\paragraph{Atomicity Garantien} \hfill \\
Einzelnes Lesen bzw. Schreiben ist atomar für
\begin{itemize}
	\itemsep -0.2em
	\item Primitive Datentypen bis 32 Bit
	\item Objekt-Referenzen
	\item long und double nur mit volatile Keyword atomar
\end{itemize}

\pagebreak

\paragraph{Beispiel} \hfill \\
Der Thread hört nicht mehr auf zu arbeiten. Das Problem ist, dass Atomarität noch keine Sichtbarkeit bedeutet. Ein Thread sieht die Änderungen eines anderen Threads eventuell nicht oder sehr viel später.

\begin{lstlisting}
class Worker extends Thread {
	private boolean doRun = true;
	@Override public void run() {
	while (doRun) { ... }
	}

	public void stopRequest() {
		doRun = false; 
	}
}
\end{lstlisting}

\subsubsection{Java Visibility Garantien}
Garantierte Sichtbarkeit zwischen den Threads.
\begin{description}
	\itemsep -0.2em
	\item [Locks Release \& Acquire:] Änderungen vor Release werden bei Acquire sichtbar.
	\item [Volatile Variable:] Änderungen bis zum Write werden beim Read sichtbar.
	\item [Thread/Task-Start] und Join: Start - Eingabe an Thread, Join - Ausgabe vom Thread
	\item [Initialisierung von final Variablen] - nach Ende des Konstruktors sichtbar.
\end{description}

\begin{figure}[H]
  \center
  \includegraphics[width=0.5\textwidth]{volatile}
  \caption{Volatile Read/Write}
\end{figure}

\subsubsection{Java Ordering Garantien}
Reihenfolge bei Sichtbarkeit (wie gehabt):
\begin{itemize}
	\itemsep -0.2em
	\item Lock -> Unlock
	\item volatile Write -> volatile Read
	\item Partielle Ordnung
\end{itemize}

\textbf{Zusätzlich} 
\begin{itemize}
	\itemsep -0.2em
	\item Synchronisationsbefehle werden zueinander nie umgeordnet
	\item Lock/Unlick, volatile-Zugriffe, Thread-Start/Join
	\item Totale Ordnun
\end{itemize}

\subsubsection{Atomare Klassen}
Sind Klassen für Boolean, Integer, Long und Reference sowie auch für Array-Elemente und offerieren diverse Atomare Operationen: \lstinline|addAndGet(), getAndAdd()|.

\begin{lstlisting}
AtomicInteger squares = new AtomicInteger(0);
squares.updateAndGet(x -> x * x);
\end{lstlisting}

\subsubsection{Optimistische Synchronisation}
Optimistische Synchronisationsverfahren gehen von der Annahme aus, daß Konflikte zwischen Transaktionen seltene Ereignisse darstellen und somit das präventive Sperren der Objekte unnötigen Aufwand verursacht. Sie prüfen erst bei Transaktionsende, ob Konflikte aufgetreten sind. Diese Operationen könne in drei Phasen unterteilt werden: Lesephase, Validierungsphase, Schreibphase.

\begin{lstlisting}
do {
	oldValue = var.get(); // lese den aktuellen Wert
	newValue = calculateChanges(oldValue);
} while (!var.compareAndSet(oldValue, newValue)); // Schreibe, falls gelesener Wert noch aktuell ist
\end{lstlisting}

\subsubsection{Lock-Free Stack (Treiber 1986)}
\begin{lstlisting}
AtomicReference<Node<T>> top = new AtomicReference<>();
...
void push(T value) {
	Node<T> newNode = new Node<>(value);
	Node<T> current;
	do {
		current = top.get(); newNode.setNext(current);
	} while (!top.compareAndSet(current, newNode));
}
\end{lstlisting}

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{nblqueue}
  \caption{Non Blocking Queue}
\end{figure}

\subsubsection{Lock-Free Data Structure}
Lock-freie Datenstrukturen sind Datenstrukturen, die ohne die Verwendung von Locks (also ohne die Verwendung von synchronized-Blöcken oder -Methoden) implementiert werden, um parallele Zugriffe auf die Datenstruktur zu synchronisieren. Sie nutzen stattdessen atomare Operationen und CAS (Compare-and-Swap) oder andere Techniken, um sicherzustellen, dass parallele Zugriffe korrekt und zuverlässig ausgeführt werden.

Ein Beispiel für eine lock-freie Datenstruktur in Java ist die Klasse ConcurrentLinkedQueue, die eine lock-freie Implementation einer Warteschlange (Queue) bereitstellt. Sie nutzt atomare Operationen und CAS, um parallele Zugriffe auf die Warteschlange zu synchronisieren.

Ein anderes Beispiel ist die Klasse AtomicInteger, die eine lock-freie Implementierung einer Integer-Variable bereitstellt. Sie nutzt atomare Operationen wie CAS, um parallele Zugriffe auf die Variable zu synchronisieren.

\pagebreak

\subsection{.NET Memory Model}
Unterschied zu Java Memory Model:
\begin{itemize}
	\itemsep -0.2em
	\item Atomicity: long/double nicht mit volatile atomar
	\item Visibility: nicht definiert, implizit durch Ordering
	\item Ordering: nur Half und Full Fences
\end{itemize}

\subsubsection{Volatile Half Fences}
Half Volatile fences in .NET sind eine Technologie, die verwendet wird, um die Synchronisierung von Schreib- und Lesezugriffen auf gemeinsam genutzten Speicher in multithreaded .NET-Anwendungen sicherzustellen. Sie sind ein Teil der Memory Model-Spezifikationen von .NET und sind ähnlich wie die Volatile-Semantik im Java Memory Model (JMM).

Half Volatile fences gibt es in zwei Arten: "acquire" und "release". Ein acquire fence verlangt, dass alle Schreibzugriffe vor dem fence sichtbar gemacht werden, bevor der Thread fortfährt. Ein release fence verlangt, dass alle Schreibzugriffe nach dem fence sichtbar gemacht werden, bevor der Thread fortfährt.

\begin{description}
	\item [Volatile Write:] Release Semantik, Vorangehende Zugriffe bleiben davor 
	\item [Volatile Read:] Acquire Semantik, Nachfolgende Zugriffe bleiben danach
\end{description}

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{dotnetvolatile}
  \caption{.NET Volatile}
\end{figure}

\subsubsection{Memory Barrier (Full Fence)} 
Memory Barriers sind spezielle Anweisungen, die dazu beitragen, die Sichtbarkeit von Schreibzugriffen auf den gemeinsamen Speicher sicherzustellen, indem sie die Reihenfolge von Anweisungen beeinflussen, die von der CPU ausgeführt werden. Sie sorgen dafür, dass Schreibzugriffe auf den Speicher sichtbar gemacht werden, bevor der Thread fortfährt und Lesezugriffe auf den aktuellen Wert des Speichers sehen.

\begin{figure}[h!]
  \center
  \includegraphics[width=0.5\textwidth]{dotnetfullfence}
  \caption{.NET Full Fence}
\end{figure}

