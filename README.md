# Paralelle Programmierung FS2022
Das Repository enthält eine Zusammenfassung, Cheatsheet sowie den Code für die Übungen und Inahlte aus dem Modul Pralelle Programmierung.


## Inhalt

* Multi-Threading Grundlagen
* Monitor Synchronisation
* Spezifische Synchronispationsprimitiven
* Die Gefahren der Nebenläufigeit
* Thread Pools
* Ansynchrone Programmierung
* Memory Modelle
* GPU Parallelisierung
* CPU Vektorparallelisierung
* MPI Cluster Parallelisierung

Verwendete Technologien:

* Java
* C#
* CUDA
* OpenMP

## Lernziele

- [ ] Konzepte Prozess und Thread
- [ ] Grundlagen der nebenläufigen Programmierung mit Java Threads