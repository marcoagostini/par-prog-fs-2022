#include <cmath>
#include <iostream>

#include "FindClostestCPU.h"
#include "float3.h"

int main() {
    int count = 10000;

    int *indexOfClosest = new int[count];
    float3 *points = new float3[count];

    for(int i = 0; i < count; i++)
    {
        points[i].x = (float)((rand() % 10000) - 5000);
        points[i].y = (float)((rand() % 10000) - 5000);
        points[i].z = (float)((rand() % 10000) - 5000);
    }

    long fastest = 1000000;
    std::cout << "We are starting the calculation of the Nearest Neighbor" << '\n';
    for (int q = 1; q <= 20; q++)
    {
        long startTime = clock();
        FindClosestCPU(points, indexOfClosest, count);
        long finishTime = clock();

        std::cout << "Run " << q << " took " << (finishTime - startTime) / 1000 << " milliseconds" << '\n';
        if((finishTime - startTime) < fastest)
        {
            fastest = finishTime - startTime;
        }
    }
    return 0;
}

