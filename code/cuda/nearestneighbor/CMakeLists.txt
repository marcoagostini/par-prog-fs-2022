cmake_minimum_required(VERSION 3.22)
project(nearestneighbor)

set(CMAKE_CXX_STANDARD 14)

add_executable(nearestneighbor
        main.cpp)
