#ifndef NEARESTNEIGHBOR_FLOAT3_H
#define NEARESTNEIGHBOR_FLOAT3_H

struct float3 {
    float x, y, z;
};

#endif //NEARESTNEIGHBOR_FLOAT3_H
