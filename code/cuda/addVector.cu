#include <stdio.h>
#include <cuda.h>

#define N 10000

__global__ 
void add(int *a, int *b, int *c)
{
    int tid = blockDim.x * blockIdx.x + threadIdx.x;
    if (tid < N) {
        c[tid] = a[tid] + b[tid];
    }

}

void print_array(int * arr, int length) 
{
    printf("[");
    for(int i=0; i<length; i++) 
    {
        printf("%i,", arr[i]);
    }
    printf("]\n");
}

int main()
{
    printf("Starting Vector Addition on GPU \n");

    int *a, *b, *c;
    int *d_a, *d_b, *d_c;

    // allocate memory on the CPU
    a = (int*) malloc( N * sizeof(int) );
    b = (int*) malloc( N * sizeof(int) );
    c = (int*) malloc( N * sizeof(int) );

    // alocate the memory on the GPU
    cudaMalloc( &d_a, sizeof(int) * N);
    cudaMalloc( &d_b, sizeof(int) * N);
    cudaMalloc( &d_c, sizeof(int) * N);

    // fill the arrays with dummy values
    for(int i=0; i<N; i++)
    {
        a[i] = i;
        b[i] = i;
    }

    print_array(a, N);
    print_array(b, N);

    // copy the arrys to the GPU
    cudaMemcpy( d_a, a, N * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy( d_b, b, N * sizeof(int), cudaMemcpyHostToDevice);

    // Execute the kernel
    int numThread = 1024;
    int numBlock = (N + numThread - 1) / numThread;

    add<<<numBlock, numThread>>>( d_a, d_b, d_c );
    cudaDeviceSynchronize();

    // copy the result back from the GPU to CPU
    cudaMemcpy( c, d_c, N * sizeof(int), cudaMemcpyDeviceToHost);
    
    print_array(c, N);

    // free memory on CPU
    free( a );
    free( b );
    free( c );

    // free memory on GPU
    cudaFree( d_a );
    cudaFree( d_b );
    cudaFree( d_c );   

    return 0; 
}


