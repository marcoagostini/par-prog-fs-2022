#include <cuda.h>
#include <iostream>

__global__ void AddIntsCUDA(int* a, int* b) 
{
    a[0] += b[0];
}

int main()
{
    int a = 5
    int b = 9;
    int *d_a, *d_b;

    // Allocate space on the device
    cudaMalloc(&d_a, sizeof(int));
    cudaMalloc(&d_b, sizeof(int));

    // Copy values from host to device
    cudaMemcpy(d_a, &a, sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, &b, sizeof(int), cudaMemcpyHostToDevice);

    // Launch the kernel
    AddIntsCUDA<<<1, 1>>>(d_a, d_b);

    // Copy values from device to host
    cudaMemcpy(&a, d_a, sizeof(int), cudaMemcpyDeviceToHost);
    std::cout << "The answer is " << a << '\n';

    cudaFree(d_a);
    cudaFree(d_b);

    return 0;
}
