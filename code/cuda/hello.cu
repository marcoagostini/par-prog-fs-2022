#include <stdio.h>
#include <cuda.h> 

// Kernel which runs on the GPU
__global__ 
void cuda_hello() {
    int index = threadIdx.x;
    int threads = blockDim.x;
    int block = blockIdx.x;
    
    printf("Hello World from GPU %i of %i in block %i \n", 
        index,
        threads, 
        block);
}

int main() {
    // Kernel Launch
    dim3 blocks = dim3(2,2);
    dim3 grids = dim3(1,1)

    cuda_hello<<<grids, blocks>>>(); 
    cudaDeviceSynchronize();

    return 0;
}

