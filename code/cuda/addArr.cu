#include <cuda.h>
#include <iostream>
#include <stdlib.h>
#include <ctime>

using namespace std;

__global__ void AddInts(int *a, int *b, int *c, int count)
{
    int id = blockIdx.x * blockDim.x + threadIdx.x;
    if(id < count) {
        c[id] = a[id] + b[id];
    }
}

int main() 
{
    srand(time(NULL));
    int count = 100;

    int *h_a = new int [count];
    int *h_b = new int [count];
    int *h_c = new int [count];

    for (int i = 0; i < count; i++) 
    {
        h_a[i] = rand() % 1000;
        h_b[i] = rand() % 1000;
    }

    cout << "Prior to Addition" << '\n';
    for (int i = 0; i < 5; i++)
    {
        cout << h_a[i] << " " << h_b[i] << "\n";
    }

    int *d_a, *d_b, *d_c;

    cudaMalloc(&d_a, sizeof(int) * count);
    cudaMalloc(&d_b, sizeof(int) * count);
    cudaMalloc(&d_c, sizeof(int) * count);

    cudaMemcpy(d_a, h_a, sizeof(int) * count, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, h_b, sizeof(int) * count, cudaMemcpyHostToDevice);

    AddInts<<<1, 256>>>(d_a, d_b, d_c, count);
    cudaDeviceSynchronize();
    
    cudaMemcpy(h_c, d_c, sizeof(int) * count, cudaMemcpyDeviceToHost); 
   
    for(int i = 0; i < 5; i++)
    {
        cout << "it's " << h_c[i] << '\n';
    }
    
    free[] h_a;
    fre[] h_b;

    cudaFree(d_a);
    cudaFree(d_b);

    return 0;
}