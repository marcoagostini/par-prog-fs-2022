﻿Task task = Task.Run(() =>
{
    Thread.Sleep(1000);
});

Task<int> task2 = Task.Run(() =>
{
    Random rn = new Random();
    return rn.Next(100);
});

Console.WriteLine("Currently waiting for the thread");

task.Wait();

Console.WriteLine("Task is done with value {0}", task2.Result);
Console.WriteLine("Task is done!");