﻿using System.Dynamic;
using System.Text;

Parallel.Invoke(
    () => RepeatAsync("Test", 10000),
    () => RepeatAsync("Quit", 10000)
);

        



Console.Write("Waiting for Task");
myTask.Delay(1000);
Console.WriteLine("done");

async Task RepeatAsync(string url, int count)
{
    StringBuilder sb = new StringBuilder();

    for(int i = 0; i < count; i++) {
        sb.Append(url);
        Console.WriteLine(sb.ToString());
    }
}

class myTask
{
    public static async void Delay(int delay)
    {
        Thread.Sleep(delay);
    }
}