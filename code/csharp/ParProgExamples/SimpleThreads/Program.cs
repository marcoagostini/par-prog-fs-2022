﻿
var myThread1 = new Thread(() => runThread(1));
var myThread2 = new Thread(() => runThread(2));

void runThread(int j)
{
    for (int i = 0; i < 100000; i++)
    {
        Console.WriteLine("MyThread {1} step {0}", i, j);
    }
}

myThread1.Start();
myThread2.Start();

myThread1.Join();
myThread2.Join();

Console.WriteLine("DONE");