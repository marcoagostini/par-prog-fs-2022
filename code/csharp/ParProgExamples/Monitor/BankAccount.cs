namespace Monitor;

using System.Threading;

public class BankAccount
{
    private decimal balance;
    private readonly object syncObject = new();

    public void Withdraw(decimal amount)
    {
        lock (syncObject);
        while (amount > balance)
        {
            Monitor.Wait(syncObject);
        }
        balance -= amount;
    }

    public void Deposit(decimal amount)
    {
        lock (syncObject)
        {
            balance += amount;
            Monitor.PulseAll(syncObject);
        }
    }
}