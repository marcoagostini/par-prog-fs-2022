﻿Task task = Task.Run(() =>
{
    Task task1 = Task.Run(() =>
    {
        Thread.Sleep(1000);
        Console.Write("Thread 1 finished\n");
    });

    Task task2 = Task.Run(() =>
    {
        Thread.Sleep(1000);
        Console.Write("Thread 2 finished\n");
    });

    task1.Wait();
    task2.Wait();
});

task.Wait();
Console.Write("We are done!\n");