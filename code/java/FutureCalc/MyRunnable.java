package FutureCalc;

public class MyRunnable implements Runnable {
    private final int upperBoundary;
    private int sum;

    public MyRunnable(int upperBoundary) {
        this.upperBoundary = upperBoundary;
    }

    @Override
    public void run() {

        for (int i = 0; i < upperBoundary; i++) {
            sum += i;
        }
    }
}
