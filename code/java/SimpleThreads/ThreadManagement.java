package SimpleThreads;

public class ThreadManagement {
    public static void main(String[] args) {
        SimpleCounter s1 = new SimpleCounter(1);
        SimpleCounter s2 = new SimpleCounter(2);
        s1.start();
        s2.start();

        try {
            s1.join();
            s2.join();
        } catch (Exception e) {

        }

    }
}
