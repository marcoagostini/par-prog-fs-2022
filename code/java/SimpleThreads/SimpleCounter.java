package SimpleThreads;

class SimpleCounter extends Thread {
    private int number;

    public SimpleCounter(int number) {
    this.number = number;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000000; i++) {
            System.out.printf("Thread %d, number %d \n", number, i);
        }
        return 0;
    }
}
