package Monitor;

public class Main {
    public static void main(String[] args) {

        Bank b1 = new Bank();

        b1.addAccount(new BankAccount("Marco", 1, 1000));
        b1.addAccount(new BankAccount("Tobias", 2, 1000));

        TransactionManager t1 = new TransactionManager(b1);
        t1.start();

        try {
            t1.join();
        } catch (Exception e) {
            System.out.print("Catched" + e);
        }

        var accounts = b1.getAccountList();

        for (BankAccount b :accounts) {
            System.out.printf("ID %d, Amount %d \n", b.getId() ,b.currentBalance());
        }


    }
}
