package Monitor;

import java.util.ArrayList;
import java.util.List;

public class Bank {
    private List<BankAccount> accountList = new ArrayList<BankAccount>();

    public synchronized void addAccount(BankAccount b) {
        accountList.add(b);
    }
    public synchronized void removeAccount(BankAccount b) {
        accountList.remove(b);
    }
    public BankAccount getAccount(int i) { return accountList.get(i); }
    public List<BankAccount> getAccountList() { return accountList; }

}