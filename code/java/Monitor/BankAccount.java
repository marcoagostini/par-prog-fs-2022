package Monitor;

public class BankAccount {
    private int balance;
    private int id;
    private String owner;

    public BankAccount(String owner, int id, int balance) {
        this.owner = owner;
        this.id = id;
        this.balance = balance;
    }

    public synchronized void depositMoney(int cash) {
        balance += cash;
        notifyAll();
    }

    public synchronized void getMoney(int cash)  {
        try {
            while (balance < cash) {
                wait();
            }
            balance -= cash;

        } catch (Exception e) {
            System.out.print("Catched!");
        }

    }

    public int currentBalance() {
        return balance;
    }

    public int getId() {
        return id;
    }
}
