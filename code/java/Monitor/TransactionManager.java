package Monitor;

public class TransactionManager extends Thread {
    private Bank bank;

    public TransactionManager(Bank b) {
        this.bank = b;
    }

    @Override
    public void run() {
        BankAccount b1 = bank.getAccount(0);
        BankAccount b2 = bank.getAccount(1);

        b1.depositMoney(100);
        b1.getMoney(100);
        b1.getMoney(100);
        b1.getMoney(100);

        b2.getMoney(1001);

        return 0;
    }
}
