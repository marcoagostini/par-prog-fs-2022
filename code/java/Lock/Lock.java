package Lock;

import Semaphore.Main;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.Thread.sleep;

public class Lock {
    public static void main(String[] args) {
        new Thread(() -> startKocking(1)).start();
        new Thread(() -> startKocking(2)).start();
        new Thread(() -> startKocking(3)).start();
        new Thread(() -> startKocking(4)).start();
    }

    public static void startKocking(int i) {
        try {
            Kitchen.status.lock();
            System.out.printf("Thread %d, kocking \n", i);
            sleep(1000);
            Kitchen.status.unlock();
            System.out.printf("Thread %d, finished! \n", i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static class Kitchen {
        public static ReentrantLock status = new ReentrantLock(true);
    }

}
