package ForkJoinPool;

import java.util.concurrent.ForkJoinPool;

public class Main {
    public static void main(String[] args) {
        int[] array = new int[] {1,2,3,4,5,6,7,8,9,10};

        ForkJoinPool threadPool = new ForkJoinPool(8);

        threadPool.invoke(new PairwiseSum(array, 0, array.length / 2));

        for(int x: array) {
            System.out.printf("%d ", x);
        }

    }
}
