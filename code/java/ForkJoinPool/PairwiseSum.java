package ForkJoinPool;

import java.util.concurrent.RecursiveAction;

class PairwiseSum extends RecursiveAction {
    private final int[] array;
    private final int lower, upper;
    private static final int THRESHOLD = 1; // configurable

    public PairwiseSum(int[] array, int lower, int upper) { this.array = array;
        this.lower = lower;
        this.upper = upper;
    }

    @Override
    protected void compute() {
        if (upper - lower > THRESHOLD) {
            int middle = (lower + upper) / 2;
            invokeAll(
                    new PairwiseSum(array, lower, middle),
                    new PairwiseSum(array, middle, upper)
            );

        } else {
            for (int i = lower; i < upper; i++) {
                array[2 * i] += array[2 * i + 1];
                array[2 * i + 1] = 0;
            }
        }
    }
}