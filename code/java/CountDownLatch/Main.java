package CountDownLatch;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class Main {
    public static void main(String[] args) {
        int NUMBER_OF_THREADS = 10;
        CountDownLatch l1 = new CountDownLatch(NUMBER_OF_THREADS);

        List<CountDown> list1 = new ArrayList<>();

        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            list1.add(new CountDown(i, l1));
        }

        for (int j = 0; j < NUMBER_OF_THREADS; j++) {
            list1.get(j).start();
        }

        try {
            l1.await();
            Thread.sleep(2500);
            System.out.print("Latch broken!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
