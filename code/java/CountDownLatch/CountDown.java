package CountDownLatch;

import java.util.concurrent.CountDownLatch;

public class CountDown extends Thread {
    private int number;
    private CountDownLatch latch;

    public CountDown(int number, CountDownLatch c1) {
        this.number = number;
        this.latch = c1;
    }

    @Override
    public void run() {
        try {
            Thread.sleep((long)Math.random() * 1000);
            latch.countDown();
            System.out.printf("Thread %d, counted down, waiting!\n", number);
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
