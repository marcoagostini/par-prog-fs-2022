package CyclicBarrier;

import java.util.concurrent.CyclicBarrier;

import static java.lang.Thread.sleep;

public class Barrier {
    public static void main(String[] args) {
        new Thread(() -> nextRound(0)).start();
        new Thread(()-> nextRound(1)).start();
        new Thread(()-> nextRound(2)).start();
        new Thread(()-> nextRound(3)).start();

    }

    public static class Round {
        public static CyclicBarrier barrier = new CyclicBarrier(1);
    }

    public static void nextRound(int i) {
        try {
            for(int j = 0; j < 10; j++) {
                sleep(1000);
                System.out.printf("Thread %d, Waiting for Barrier to fall... \n", i);
                Round.barrier.await();
                System.out.print("Barrier has fallen!\n");
            }
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
}
