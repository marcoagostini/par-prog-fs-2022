package Semaphore;

import java.util.concurrent.Semaphore;

import static java.lang.Thread.sleep;

public class Main {
    public static void main(String[] args) {

        new Thread(() -> startKocking(1)).start();
        new Thread(() -> startKocking(2)).start();
        new Thread(() -> startKocking(3)).start();
        new Thread(() -> startKocking(4)).start();
    }

    public static void startKocking(int i) {
        try {
            Kitchen.status.acquire(1);
            System.out.printf("Thread %d, kocking \n", i);
            sleep(1000);
            Kitchen.status.release(1);
            System.out.printf("Thread %d, finished! \n", i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static class Kitchen {
        public static Semaphore status = new Semaphore(1, true);
    }
}
