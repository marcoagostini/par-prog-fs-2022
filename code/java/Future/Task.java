package Future;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

public class Task {
    public static void main(String[] args) {

        var threadPool = new ForkJoinPool();

        var simpleTask = threadPool.submit(new simpleTask());
        var advancedTask = threadPool.submit(new advancedTask());

        try {
            simpleTask.get();
            System.out.print(advancedTask.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public static class simpleTask implements Runnable {
        @Override
        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.print("simpleTask finised\n");
            return 0;
        }
    }

    public static class advancedTask implements Callable<Integer> {

        @Override
        public Integer call() throws Exception {
            System.out.print("adnvacedTask finished\n");
            return 100;
        }
    }

}
