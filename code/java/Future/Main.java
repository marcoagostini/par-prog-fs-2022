package Future;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;

import static java.lang.Thread.sleep;

public class Main {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        var threadPool = new ForkJoinPool();

        var left = threadPool.submit(() -> {
            try {
                sleep(1000);
            } catch (Exception e) {
                System.out.print("Catched this bad boy!");
            }
            return 1;
        });



        System.out.print("Done " + left.get());
    }
}
