#include <stdio.h> 
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "mpi.h" 
#include "Bitmap.h"

#define OUTPUT_FILE "juliaset.bmp"
#define IMAGE_WIDTH (4 * 1024)
#define IMAGE_HEIGHT (4 * 1024)
#define IMAGE_SCALE 1.5

typedef struct {
	double real, imag;
} Complex;

Complex multiply(Complex a, Complex b) {
	Complex c;
	c.real = a.real * b.real - a.imag * b.imag;
	c.imag = a.real * b.imag + a.imag * b.real;
	return c;
}

Complex add(Complex a, Complex b) {
	Complex c;
	c.real = a.real + b.real;
	c.imag = a.imag + b.imag;
	return c;
}

int julia_value(int x, int y) {
	double jx = IMAGE_SCALE * ((double)IMAGE_WIDTH / 2 - x) / (IMAGE_WIDTH / 2);
	double jy = IMAGE_SCALE * ((double)IMAGE_HEIGHT / 2 - y) / (IMAGE_HEIGHT / 2);

	Complex c, a;
	c.real = -.8f;
	c.imag = .156f;
	a.real = jx;
	a.imag = jy;
	
	int i;
	for (i = 0; i < 200 && a.real * a.real + a.imag * a.imag <= 1000; i++) {
		a = add(multiply(a, a), c);
	}
	return i;
}

void compute_julia_set(int* pixels) {


	int row;
	for (row = 0; row < IMAGE_HEIGHT; row++) {
		int col;
		for (col = 0; col < IMAGE_WIDTH; col++) {
			pixels[row * IMAGE_WIDTH + col] = julia_value(col, row);
		}
	}
}

int main(int argc, char* argv[]) {
	clock_t start = clock();
	int* pixels = (int*)malloc(IMAGE_HEIGHT * IMAGE_WIDTH * sizeof(int));
	compute_julia_set(pixels);
	clock_t end = clock();
	double elapsed_time = ((double)end - start) * 1000 / (double)CLOCKS_PER_SEC;
	printf("Total computation: %0.f ms\n", elapsed_time);
	write_bitmap_image(pixels, IMAGE_WIDTH, IMAGE_HEIGHT, OUTPUT_FILE);
	free(pixels);
	return 0;
}
