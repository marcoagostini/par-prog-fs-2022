// Partially based on https://stackoverflow.com/a/2654860

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#pragma warning(disable:4996)

void write_bitmap_image(int* pixels, int width, int height, const char* filename) {
	unsigned char *image = (unsigned char *)malloc(3 * width * height);;
	memset(image, 0, 3 * width * height);
	int row;
	for (row = 0; row < height; row++)
	{
		int col;
		for (col = 0; col < width; col++)
		{
			int x = col;
			int y = (height - 1) - row;
			unsigned int value = (unsigned int)pixels[row * width + col];
			unsigned char red = value % 255;
			unsigned char green = value % 127;
			unsigned char blue = value % 63;
			image[(x + y * width) * 3 + 2] = (unsigned char)(red);
			image[(x + y * width) * 3 + 1] = (unsigned char)(green);
			image[(x + y * width) * 3 + 0] = (unsigned char)(blue);
		}
	}

	unsigned char bmpfileheader[14] = { 'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0 };
	unsigned char bmpinfoheader[40] = { 40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0 };
	unsigned char bmppad[3] = { 0,0,0 };

	int filesize = 54 + 3 * width * height;
	bmpfileheader[2] = (unsigned char)(filesize);
	bmpfileheader[3] = (unsigned char)(filesize >> 8);
	bmpfileheader[4] = (unsigned char)(filesize >> 16);
	bmpfileheader[5] = (unsigned char)(filesize >> 24);

	bmpinfoheader[4] = (unsigned char)(width);
	bmpinfoheader[5] = (unsigned char)(width >> 8);
	bmpinfoheader[6] = (unsigned char)(width >> 16);
	bmpinfoheader[7] = (unsigned char)(width >> 24);
	bmpinfoheader[8] = (unsigned char)(height);
	bmpinfoheader[9] = (unsigned char)(height >> 8);
	bmpinfoheader[10] = (unsigned char)(height >> 16);
	bmpinfoheader[11] = (unsigned char)(height >> 24);

	FILE* file = fopen(filename, "wb");
	fwrite(bmpfileheader, 1, 14, file);
	fwrite(bmpinfoheader, 1, 40, file);
	for (row = 0; row < height; row++)
	{
		fwrite(image + (width * (height - row - 1) * 3), 3, width, file);
		fwrite(bmppad, 1, (4 - (width * 3) % 4) % 4, file);
	}

	free(image);
	fclose(file);
}