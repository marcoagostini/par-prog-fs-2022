#include <stdio.h>
#include "mpi.h"

int main(int argc, char * argv[]) 
{
    int rank, size;

    // Broadcast arguments to processes
    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // main task to execute
    if (rank == 0) 
    {
        // Sender process == 0
        int value = rand();
        int to;

        for (to = 1; to < size; to++)
        {
            MPI_Send(&value, 1, MPI_INT, to, 0, MPI_COMM_WORLD);
        } 
    } 
    else 
    {
        // all other processes
        int value;
        MPI_Recv(&value, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("%i received by %i \n", value, rank);
    }
    
    // clean up MPI Environement
    MPI_Finalize();
}
